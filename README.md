# README #

This repository contains files for Pepper demo.

### Set up ###

Set MODIM_HOME environment variable to respective folder.


### How to start the demo ###

 1. Run MODIM WS server
    

    On laptop and Pepper:

        cd $MODIM_HOME/src/GUI
        python ws_server.py [-robot pepper]

    
 2. Start HTTP web server and browser
    
    On laptop and Pepper:
    

        cd <demo_folder>
        python -m SimpleHTTPServer 8000

        
    On Pepper (local MODIM):
    

        cd pepper_tools/tablet
        python show_web.py --url <demo_folder>/index.html

    
    On Pepper (remote MODIM) or other client:
    
        firefox http://<IP_HTTPServer>:8000


3. Run the demo client

        cd <demo_folder>/scripts
        python <demo_file>


### Remote control ###

Connect from a browser to


    http://<IP_HTTPServer>:8000/m.html


and use buttons to drive the application.



### How to use joystick to control events ###

1. Run joy ROS node

        roslaunch joy.launch



2. Link joystick buttons to events in a demo script


        import joystick

        event_map = { 'BLUE':'back' ,'GREEN':'', 'RED':'next', 'ORANGE':'', 'LB':'','RB':'','LT':'','RT':'','BACK':'back','START':'home'}

        def joystick_cb():
            global event_map
            print joystick.lastevent()
            csend_noblock('*'+event_map[joystick.lastevent()])

        joystick.start(joystick_cb)



    Callback will be called upon joystick input and the event associated to the button through ```event_map``` will be fired.




