# security alert demo

#export PYTHONPATH=$PYTHONPATH:$MODIM_HOME/src/GUI:$MODIM_HOME/src/action
#python -m ws_server -robot pepper

import os
import sys
import time
#import joystick
import random

if (os.getenv("MODIM_HOME")==None):
    print("Plaese set MODIM_HOME environment variable")
    sys.exit(0)


modimdir = os.getenv('MODIM_HOME')+'/src/GUI'

sys.path.append(modimdir)

from ws_client import *
import ws_client


def reset():

    begin()

    im.display.loadUrl('index.html')
    im.robot.setLanguage('Italian')
    im.robot.normalPosture()

    end()



def start():

    begin()

    im.setDemoPath('/home/nao/spqrel_app/html/mf18')
    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('index.html')
    im.robot.normalPosture()

    end()


def slides():

    QuizJunior = [4, 5, 6, 8, 9, 11, 12, 13]
    QuizSenior = [15, 16, 17, 18, 19]
    English = [21, 22, 29, 30, 31]
    Barzellette = [ 25, 26, 27, 28 ]

    slide_intro = 1
    slide_cr = 2
    slide_correct = 23
    slide_wrong = 24


    begin()

    im.setPath('/home/nao/spqrel_app/html/mf18')
    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('slide.html')
    im.robot.normalPosture()
    time.sleep(1)
    a = ''
    while a!='timeout':
        im.setProfile(['*', '*', 'it', '*'])
        im.display.remove_buttons()
        a = im.ask('slide%d' %slide_intro, 60)
        if a=='barz':
            im.execute('slide%d' %random.choice(Barzellette))
        elif a=='quizj':
            im.execute('slide%d' %random.choice(QuizJunior))
        elif a=='quizs':
            im.execute('slide%d' %random.choice(QuizSenior))
        elif a=='english':
            im.setProfile(['*', '*', 'en', '*'])
            im.execute('slide%d' %random.choice(English))
        elif a=='cr':
            im.execute('slide%d' %slide_cr)

        im.robot.normalPosture()
        time.sleep(1)

    end()




if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("interaction", type=str,  help="interaction to run")

    args = parser.parse_args()

    mws = ModimWSClient()
    mws.cconnect()
    
    f = eval(args.interaction)

    mws.run_interaction(f)

