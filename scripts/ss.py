# ciao demo

import os
import sys
import time
import random
import argparse

try:
    sys.path.insert(0, os.getenv('MODIM_HOME')+'/src/GUI')
except Exception as e:
    print "Please set MODIM_HOME environment variable to MODIM folder."
    sys.exit(1)

import ws_client
from ws_client import *


def startauto():
    c = "/home/nao/bin/tmux new-window -n 'script'"
    os.system(c)
    time.sleep(1)
    c = "/home/nao/bin/tmux send-keys -t init \"cd /home/nao/spqrel_app/html/mf18/scripts\" C-m"
    os.system(c)
    time.sleep(1)
    c = "/home/nao/bin/tmux send-keys -t init \"python ss_auto.py\" C-m"
    os.system(c)


def intro():

    begin()

    im.robot.showurl('mf18/slideimg.html')

    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('slideimg.html')

    im.robot.setAlive(True)

    im.execute('ss1', audio=False)

    im.robot.normalPosture()

    im.robot.startFaceDetection()

    time.sleep(3)

    c = "/home/nao/bin/tmux new-window -n 'script'"
    os.system(c)
    time.sleep(1)
    c = "/home/nao/bin/tmux send-keys -t init:script \"cd /home/nao/spqrel_app/html/mf18/scripts\" C-m"
    os.system(c)
    time.sleep(1)
    c = "/home/nao/bin/tmux send-keys -t init:script \"python ss_auto.py\" C-m"
    os.system(c)

    end()


def presentazione():

    begin()

    im.robot.showurl('mf18/slide2.html')

    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('slide2.html')

    im.robot.setAlive(True)

    im.execute('ss1', audio=False)

    im.robot.normalPosture()

    time.sleep(2)

    for i in range(1,5):    
        time.sleep(1)
        im.execute('ss%d' %i)
        time.sleep(1)
    
    im.execute('ss1', audio=False)

    im.robot.normalPosture()
 
    end()


def web():
    im.robot.showurl('http://www.saporisegreti.it')

def ears():
    im.robot.ears_led(True)
    time.sleep(2)
    im.robot.ears_led(False)


def head():
    im.robot.headPose(0.0, -0.2, 1.0)


def gitpull():
    im.gitpull()


def logoSS():
    im.robot.showurl('mf18/slideimg.html')
    im.display.loadUrl('slideimg.html')
    im.execute('ss1', audio=False)


def logoDIAG():
    im.robot.showurl('mf18/slideimg.html')
    im.display.loadUrl('slideimg.html')
    im.execute('logoDIAG', audio=False)


def imageB():
    im.robot.showurl('mf18/img/sapsegr_bambini.jpg')

def imageG():
    im.robot.showurl('mf18/img/sapsegr_gelato.jpg')

def imageT():
    im.robot.showurl('mf18/img/sapsegr_tartufo.jpg')

def imageF():
    im.robot.showurl('mf18/img/sapsegr_caffe.jpg')



def image(rim=None):

    images = ['bambini','gelato','tartufo','caffe','cook','leonardo']

    im.robot.showurl('mf18/img/sapsegr_banner.jpg')

    time.sleep(3)

    if rim is None:
        rim = random.choice(images)

    im.robot.showurl('mf18/img/sapsegr_%s.jpg' %rim)

    time.sleep(5)

    im.robot.showurl('mf18/img/sapsegr_mappa.png')


def chef(ix):
    print('ciao %d' %ix)
    #im.robot.showurl('mf18/img/sschef/Schede_Monitor-%02d.jpg' %i)


def ciao():
    print('ciao')


def log():
    im.robot.logenable(enable=True)


def nolog():

    im.robot.logenable(enable=False)





if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", type=str, default='intro', help="Interaction function")
    parser.add_argument('--local', help='Local MODIM connection', action='store_true')

    args = parser.parse_args()
    interaction = eval(args.i)

    mws = ModimWSClient()

    if args.local:
        # local execution
        mws.setDemoPathAuto(__file__)
        mws.setCmdServerAddr('127.0.0.1')
    else:
        # remote execution
        # Set MODIM_IP to connnect to remote MODIM server
        # Use absolute path of the demo folder
        mws.setDemoPath('/home/nao/spqrel_app/html/mf18')


    mws.run_interaction(interaction)

