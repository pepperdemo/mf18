# Maker Faire demo

#export PYTHONPATH=$PYTHONPATH:$MODIM_HOME/src/GUI:$MODIM_HOME/src/action
#python -m ws_server -robot pepper

import os
import sys
import time
import argparse
import random

if (os.getenv("MODIM_HOME")==None):
    print("Plaese set MODIM_HOME environment variable")
    sys.exit(0)


modimdir = os.getenv('MODIM_HOME')+'/src/GUI'

sys.path.append(modimdir)

from ws_client import *
import ws_client

def slides():
    global idslide
    
    begin()
    #im.setPath('/home/nao/spqrel_app/html/mf18')
    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('slide.html')
    #im.robot.normalPosture()

    im.execute('slide%d' %idslide)

    im.robot.normalPosture()

    end()


if __name__ == "__main__":
    cmdsever_ip = '127.0.0.1'
    cmdserver_port = 9101
    demo_ip = '127.0.0.1'
    demo_port = 8000

    parser = argparse.ArgumentParser()
    parser.add_argument("idslide", type=int, default='1',  help="slide number")

    args = parser.parse_args()


    mws = ModimWSClient()
    mws.setCmdServerAddr(cmdsever_ip, cmdserver_port) 
    mws.setDemoServerAddr(demo_ip, demo_port)
    if (os.getenv("PEPPER_DEMOS_DIR")!=None):
        mws.setDemoPath(os.getenv('PEPPER_DEMOS_DIR')+'/edurobotics18')

    # mws.store_interaction(isurveillance)


    mws.setGlobalVar('idslide', args.idslide)

    mws.run_interaction(slides)


